<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class OrderDetailController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function report(){
        $start = Input::get('startDate');
        $end = Input::get('endDate');
        $reports = [];

        if($start and $end) {
            $reports = DB::table('products')
                ->join('order_details', 'order_details.id_produk', '=', 'products.id')
                ->join('orders', 'order_details.id_order', '=', 'orders.id' )
                ->where('tanggal', '>=', $start)->where('tanggal', '<=', $end)
                ->groupBy('nama')
                ->get(['nama', 'warna', 'ukuran', 'tanggal', DB::raw('sum(harga) as harga'), DB::raw('count(*) as total')]);
        }

        return view('report.index')->with('reports', $reports);
	}

    /*
    public function search(){
        $start = Input::get('starDate');
        $end = Input::get('endDate');

        if($start && $end) {
            $report = DB::table('products')
                ->join('order_details', 'order_details.id_produk', '=', 'products.id')
                ->join('orders', 'order_details.id_order', '=', 'orders.id' )
                ->where('tanggal', '>=', $start)->where('tanggal', '<=', $end)
                ->get();
        }

        $result
    }
    */

}
