<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('order.index')->with('orders', Order::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('order.create')->with('products', Product::all());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

        $validator = Validator::make(Input::all(), Order::$rules);

        if($validator->passes()) {
            $order = new Order;
            $order->tanggal = Input::get('tanggal');
            $order->order_number = Input::get('order_number');
            $order->order_total = Input::get('order_total');
            $order->save();

            return Redirect::to('admin/order')
                ->with('message', 'Order telah dibuat');
        }

        return Redirect::to('admin/order/create')
            ->with('message', "Oops, ada yang salah dengan pengisian")
            ->withErrors($validator)
            ->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $order = Order::findOrFail($id);
        $orderdetails = OrderDetail::join('products', 'order_details.id_produk', '=', 'products.id')->where('id_order', '=', $id)->get();
        return view('order.show', compact('order'))->with('orderdetails', $orderdetails);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $order = Order::findOrFail($id);

        $order->delete();

        return Redirect::to('admin/order')
            ->with('message', 'Produk telah dihapus');
	}

}
