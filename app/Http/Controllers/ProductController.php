<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class ProductController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    public function __construct(){
        $this->middleware('auth');
    }
     * /

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('product.index')->with('products', Product::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('product.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Product::$rules);

        if($validator->passes()) {
            $product = new Product;
            $product->nama = Input::get('nama');
            $product->warna = Input::get('warna');
            $product->ukuran = Input::get('ukuran');
            $product->stok = Input::get('stok');
            $product->harga = Input::get('harga');

            $image = Input::file('image');
            $filename = date('Y-n-d') . "-" . $image->getClientOriginalName();
            $path = public_path('img/products/' . $filename);
            Image::make($image->getRealPath())->resize(468, 291)->save($path);
            $product->image = 'img/products/'.$filename;
            $product->save();

            return Redirect::to('admin/product')
                ->with('message', 'Product telah dibuat');
        }

        return Redirect::to('admin/product/create')
            ->with('message', "Oops, ada yang salah dengan pengisian")
            ->withErrors($validator)
            ->withInput();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$product = Product::findOrFail($id);

        return view('product.show', compact('product'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $product = Product::findOrFail($id);

        return view('product.edit', compact('product'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
        $product = Product::findOrFail($id);

        $product->update($request->all());

        return Redirect::to('admin/product');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$product = Product::findOrFail($id);

        File::delete('public/'. $product->image);
        $product->delete();

        return Redirect::to('admin/product')
            ->with('message', 'Produk telah dihapus');
	}

}
