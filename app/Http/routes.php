<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Product;
use App\Order;
use App\OrderDetail;

Route::resource('admin/product', 'ProductController');
Route::resource('admin/order', 'OrderController');

Route::get('admin/report', 'OrderDetailController@report');
Route::get('admin', 'HomeController@admin');

Route::get('/', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
