<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tanggal', 'order_number', 'order_total'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function orderDetails(){
        return $this->hasMany('App\OrderDetail');
    }

    public static $rules = array(
        'tanggal' => 'required',
        'order_number' => 'required|unique:orders',
        'order_total' => 'required',
    );

}
