<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_details';

    /**
     * The attributes excluded from mass assignment.
     *
     * @var array
     */
    protected $guarded = array("id_order", "id_produk");

    /**
     * Each order item belongs to EXACTLY one order.
     *
     * @var array
     */
    public function order() {
        return $this->belongsTo('App\Order');
    }


    /**
     * Each order item belongs EXACTLY one product.
     *
     * @var array
     */
    public function products() {
        return $this->belongsTo('App\Product');
    }

}
