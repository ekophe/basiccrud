<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama', 'warna', 'ukuran', 'stok', 'harga', 'image'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Each product can be used (referenced) by many order items.
     *
     * @var array
     */
    public function orderDetail() {
        return $this->hasMany('App\OrderDetail');
    }

    public static $rules = array(
        'nama' => 'required',
        'warna' => 'required',
        'ukuran' => 'required',
        'stok' => 'required',
        'harga' => 'required',
        'image' => 'required|image|mimes:jpeg,jpg,bmp,png.gif'

    );

}
