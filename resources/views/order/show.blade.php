
@extends('layout.main')

@section('content')

    <h1>Order Detail</h1>
    <div id="order-details">
        <h1>Nama: {{$order->tanggal}}</h1>
        <p>Ukuran : {{$order->order_number}}</p>
        <p>Warna: {{$order->order_total}}</p>

    </div><!-- end product-details -->

    <h2>Produk yang dipilih</h2>
    <div id="product-order-list">
    <table border="1" class="table table-striped">
    		<tr>
    			<th>Nama Produk</th>
    			<th>Warna</th>
    			<th>Ukuran</th>
    			<th>Harga</th>
    		</tr>

    		@foreach($orderdetails as $orderdetail)
    		<tr>
    		<td>{!!$orderdetail->nama!!}</td>
            <td>{!!$orderdetail->warna!!}</td>
            <td>{!!$orderdetail->ukuran!!}</td>
            <td>{!!$orderdetail->harga!!}</td>
    		@endforeach

    	</table>
    </div>

@stop
