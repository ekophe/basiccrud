@extends('layout.main')

@section('content')

<div id="produk-index">
	<h1>List Order Nice Shop</h1>

	<table border="1" class="table table-striped">
		<tr>
			<th>Tanggal Order</th>
			<th>Order Number</th>
			<th>Order Total</th>
            <th>Order Detail</th>

		</tr>

		@foreach($orders as $order)
			<tr>
				<td>{!!$order->tanggal!!}</td>
				<td>{!!$order->order_number!!}</td>
				<td>{!!$order->order_total!!}</td>
				<td>
                    {!! Form::open(array('url' => 'admin/order/' . $order->id, 'method' => 'DELETE')) !!}
                    <a href="/admin/order/{{$order->id}}" class="btn btn-primary">Detail</a>
                    {!!Form::submit('Delete', array('class'=>'btn btn-primary'))!!}
                    {!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop