@extends('layout.main')

@section('content')

    <h1> Create a Product</h1>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
        	    <strong>Whoops!</strong> Ada yang salah dengan input.<br><br>
        		<ul>
        		    @foreach ($errors->all() as $error)
        			    <li>{{ $error }}</li>
        			@endforeach
        		</ul>
        	</div>
        @endif
    {!!Form::open(array('url'=>'admin/order'))!!}
    <div class="form-group">
        {!!Form::label('tanggal', 'Tanggal', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::input('date', 'tanggal', null, ['class' => 'form-control', 'placeholder' => 'Date']);!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('order_number', 'Order Number', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
           {!!Form::text('order_number', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('order_total', 'Order Total', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::text('order_total', null, array('class'=>'form-control'))!!}
        </div>
    </div>
    <div class="form-group">
        <h2 class="col-md-4 control-label">Product</h2>
    </div>

    <table border="1" class="table table-striped">
		<tr>
			<th></th>
			<th>Nama</th>
			<th>warna</th>
			<th>ukuran</th>
			<th>stok</th>
            <th>harga</th>

		</tr>
    <br>
		@foreach($products as $product)
			<tr>
				<td></td>
				<td>{!!$product->nama!!}</td>
				<td>{!!$product->warna!!}</td>
				<td>{!!$product->ukuran!!}</td>
				<td>{!!$product->stok!!}</td>
				<td>{!!$product->harga!!}</td>
			</tr>
		@endforeach
	</table>

    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!!Form::submit('Create Order', array('class'=>'secondary-cart-btn'))!!}
    </div>
    </div>
    {!!Form::close()!!}


@stop