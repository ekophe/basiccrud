@extends('layout.main')

@section('content')

<div id="produk-index">
	<h1>List Produk yang telah dibeli</h1>

	{!! Form::open(['method' => 'GET']) !!}

    <div class="form-group">
        {!!Form::label('startDate', 'Start Date', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::input('date', 'startDate', null, ['class' => 'form-control', 'placeholder' => 'Date']);!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('endDate', 'End Date', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::input('date', 'endDate', null, ['class' => 'form-control', 'placeholder' => 'Date']);!!}
        </div>
    </div>

    <div class="col-md-6 col-md-offset-4">
            {!!Form::submit('Search', array('class'=>'secondary-cart-btn'))!!}
    </div>
	{!! Form::close() !!}



	<table border="1" class="table table-striped">
		<tr>
			<th>Nama</th>
			<th>warna</th>
			<th>ukuran</th>
            <th>total Harga</th>
            <th>Total</th>

		</tr>

		@foreach($reports as $report)
			<tr>
				<td>{!!$report->nama!!}</td>
				<td>{!!$report->warna!!}</td>
				<td>{!!$report->ukuran!!}</td>
				<td>{!!$report->harga!!}</td>
				<td>{!!$report->total!!}</td>

			</tr>
		@endforeach
	</table>

</div>
@stop