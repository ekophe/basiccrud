@extends('layout.main')

@section('content')

<div id="produk-index">
	<h1>List Produk Nice Shop</h1>

	<table border="1" class="table table-striped">
		<tr>
			<th>Nama</th>
			<th>warna</th>
			<th>ukuran</th>
            <th>harga</th>
            <th></th>
		</tr>

		@foreach($products as $product)
			<tr>
				<td>{!!$product->nama!!}</td>
				<td>{!!$product->warna!!}</td>
				<td>{!!$product->ukuran!!}</td>
				<td>{!!$product->harga!!}</td>
                <td>
                    {!! Form::open(array('url' => 'admin/product/' . $product->id, 'method' => 'DELETE')) !!}
                    <a href="/admin/product/{{$product->id}}/edit" class="btn btn-primary">Edit</a>
                    <a href="/admin/product/{{$product->id}}" class="btn btn-primary">Detail</a>
                    {!!Form::submit('Delete', array('class'=>'btn btn-primary'))!!}
                    {!! Form::close() !!}

				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop