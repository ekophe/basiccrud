
@extends('layout.main')

@section('content')

    <div id="product-image">
        {!!Html::image($product->image, $product->title)!!}
    </div><!-- end product-image -->
    <div id="product-details">
        <h1>Nama: {{$product->nama}}</h1>
        <p>Ukuran : {{$product->ukuran}}</p>
        <p>Warna: {{$product->warna}}</p>
        <p>Stok: {{$product->stok}}</p>
        <p>Harga: Rp. {{$product->harga}}</p>
    </div><!-- end product-details -->

@stop