@extends('layout.main')

@section('content')

    <h1> Create a Product</h1>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
        	    <strong>Whoops!</strong> Ada yang salah dengan input.<br><br>
        		<ul>
        		    @foreach ($errors->all() as $error)
        			    <li>{{ $error }}</li>
        			@endforeach
        		</ul>
        	</div>
        @endif
    {!!Form::open(array('url'=>'admin/product', 'files'=>true))!!}
    <div class="form-group">
        {!!Form::label('nama', 'Nama', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::text('nama', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('warna', 'Warna', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
           {!!Form::text('warna', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('ukuran', 'Ukuran', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::text('ukuran', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('stok', 'Stok', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::text('stok', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('harga', 'Harga', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
            {!!Form::text('harga', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('image', 'Pilih Gambar', array('class'=>'col-md-4 control-label'))!!}
        <div class="col-md-6">
        {!!Form::file('image', null, array('class'=>'form-control'))!!}
        </div>
    </div>

    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!!Form::submit('Create Product', array('class'=>'secondary-cart-btn'))!!}
    </div>
    </div>

    {!!Form::close()!!}

@stop